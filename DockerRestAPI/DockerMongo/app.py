import flask
import os
from flask import Flask, request, render_template, json
from pymongo import MongoClient

import acp_times
import config

###
# Globals
###
app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(host="db", port=27017)
db = client.brevet


###
# Pages
###
@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


@app.errorhandler(500)
def page_not_found(error):
    app.logger.info("System internal error.")
    return flask.jsonify({"success": False, "error": "Internal error."})


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km

    # default info
    date = request.args.get('date', "", type=str)
    time = request.args.get('time', "", type=str)
    dist = request.args.get('dist', 200, type=int)
    message = ""

    control_monitor = 0
    if km == 0:
        message = "Warning! Control distance should not be ZERO!"
    else:
        control_monitor = (km - dist) / dist
    if control_monitor > 0.2:
        message = "Warning! Control distance is 20% or longer than brevet!"

    times = date + " " + time
    open_time = acp_times.open_time(km, 200, times)
    close_time = acp_times.close_time(km, 200, times)
    result = {"error_msg": message, "open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route('/json-brevet')
def json_brevet():
    _items = db.time_control.find()
    items = []
    for item in _items:
        del item["_id"]
        items.append(item)
    return flask.jsonify(items)


@app.route('/brevet')
def brevet():
    _items = db.time_control.find()
    items = [item for item in _items]
    return render_template('brevet.html', items=items)


params = ["distance", "date", "time", "control_time_list"]
control_time_keys = ["miles", "km", "open", "close"]  # no need for location


@app.route('/new', methods=['POST'])
def new():
    for param in params:
        if request.form.get(param, "") == "":
            return flask.jsonify({"success": False, "error": "Param {} is required.".format(param)})

    item_doc = {
        "distance": request.form['distance'],
        "date": request.form['date'],
        "time": request.form['time'],
        "control_time_list": request.form['control_time_list'],
    }
    item_doc["control_time_list"] = json.loads(item_doc["control_time_list"])
    if not item_doc["control_time_list"]:
        return flask.jsonify({"success": False, "error": "Param control_time_list is required."})

    for control_time in item_doc["control_time_list"]:
        for param in control_time_keys:
            if param not in control_time or control_time[param] == "":
                return flask.jsonify({"success": False, "error": "Param {} is required.".format(param)})

    db.time_control.insert_one(item_doc)

    return flask.jsonify({"success": True})


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
