# Laptop Service

from flask import Flask
from flask_restful import Resource, Api
import os
import io
import csv
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import json
from bson import ObjectId

# Instantiate the app
app = Flask(__name__)
api = Api(app)
client = MongoClient("DB_PORT_27017_TCP_ADDR", 27017)
db = client.brevet

class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }

# Create routes
# Another way, without decorators
class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


class listAll(Resource):
	def get(self):
		_items = db.brevet.find()

		items = [item for item in _items]
		for item in items:
			item.pop("_id")
		app.logger.debug(items)
		return JSONEncoder().encode(items)


class listOpenOnly(Resource):
	def get(self):
		_items = db.brevet.find()
		items = [item for item in _items]
		for item in items:
			item.pop("_id")
			item.pop("close")
		app.logger.debug(items)
		return JSONEncoder().encode(items)


class listCloseOnly(Resource):
	def get(self):
		_items = db.brevet.find()
		items = [item for item in _items]
		for item in items:
			item.pop("_id")
			item.pop("open")
		app.logger.debug(items)
		return JSONEncoder().encode(items)


api.add_resource(listAll, '/listAll', '/listAll/json')
api.add_resource(listOpenOnly, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(listCloseOnly, '/listCloseOnly', '/listCloseOnly/json')



# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
